import requests
import json
import logging as log
from requests.exceptions import HTTPError, InvalidURL, ConnectionError, Timeout, RequestException

"""
API: https://secure.meetup.com/meetup_api/
Pre-requisites:-
    1. Python(3.x)
    2. pip
    3. meetup.api (pip install)
Authors: Harsh Rajpal, Raj Thakur
"""


def is_valid_member_id(member_id: str, api_key: str, base_url='https://api.meetup.com/'):
    """
    This method checks if the specified member_id is a valid meetup user id or not
    :param member_id: member id of the member as available on url: 'https://www.meetup.com/profile'
    :param api_key: API Key of the user polling the results using this function.
    :returns: True if the user's meetup id is valid, False, if the id is not valid, None, otherwise.
    """
    payload = {'key': api_key}
    try:
        res = requests.get(base_url + f'members/{member_id}', params=payload, timeout=3)
        if res.status_code == 200:
            return True
        elif res.status_code == 400:
            return False
        else:
            return None
    except HTTPError as e:
        print("Http Error")
        log.error(e)
    except ConnectionError as e:
        print("Connection Error")
        log.error(e)
    except Timeout as e:
        print("Timeout Error:")
        log.error(e)
    except RequestException as e:
        print("Oops: Something Else")
        log.error(e)


def is_part_of_group(member_id: str, group_name: str, api_key: str, base_url='https://api.meetup.com/'):
    """
    This method checks if the specified member_id is a part of the group or not
    :param member_id: id of the member as available on url: https://meetup.com/profile
    :param group_name: Name of the group for which the details are to be fetched, e.g. 'PythonPune'
    :param api_key: API key of the user polling the results using this function
    :param base_url: Base url of the page from which the details are to be fetched. Default='https://api.meetup.com/'
    :returns: True if the meetup-id is a part of the group, False, if the id is not a part of group, None, otherwise.
    """
    payload = {'key': api_key}
    try:
        res = requests.get(base_url + f'/{group_name}/members/{member_id}', params=payload, timeout=3)
        if res.status_code == 200:
            return True
        if res.status_code == 400:
            res_content = json.loads(res.content)
            error_code = res_content['errors'][0]['code']
            if error_code == 'member_error':
                print("Member Not of group")
                return False
            else:
                return None
        if res.status_code == 404:
            res_content = json.loads(res.content)
            print(res_content['errors'][0]['message'])
            return None
    except HTTPError as e:
        print("Http Error")
        log.error(e)
    except ConnectionError as e:
        print("Connection Error")
        log.error(e)
    except Timeout as e:
        print("Timeout Error:")
        log.error(e)
    except RequestException as e:
        print("Oops: Something Else")
        log.error(e)


def has_an_event(group_url: str, api_key: str, base_url='https://api.meetup.com/'):
    """
    API:https://api.meetup.com/:urlname/events
    :param group_url: url-name of the group as available on meetup.com
    :param api_key: API Key of the user polling the event_id using this function
    :param base_url: base url of the meetup API, default_url = 'https://api.meetup.com/'
    :returns: event id of the upcoming/currently happening event for the specified group, None, otherwise.
    """
    payload = {'key': api_key}
    try:
        res = requests.get(base_url + f'/{group_url}/events', params=payload, timeout=3)
        if res.status_code == 200:
            res_content = json.loads(res.content)
            if len(res_content) is not 0:
                return res_content[0].get('id')
            else:
                return None
        elif res.status_code == 400:
            raise InvalidURL
        elif res.status_code == 404:
            raise ConnectionError(res.status_code)
    except (HTTPError, InvalidURL, ConnectionError) as e:
        res_content = json.loads(res.content)
        log.error(res_content['errors'][0]['message'])


def poll_rsvps(group_url: str, event_id: str, api_key: str, base_url='https://api.meetup.com/'):
    """
    This method returns the list of meetup_id:name for the rsvps to a particular event.
    API: https://www.meetup.com/meetup_api/docs/:urlname/events/:event_id/rsvps/#list
    Sample url: https://api.meetup.com/:urlname/events/:event_id/rsvps
    :param group_url: url-name of the group as available on meetup.com
    :param event_id: id of the event(currently-happening/upcoming) held by the group url
    :param api_key: API Key of the user polling the event_id using this function
    :param base_url: base url of the meetup API, default_url = 'https://api.meetup.com/'
    :returns: list of meetup-id:username for the users with rsvps if any, None, otherwise
    """
    payload = {'key': api_key}
    try:
        res = requests.get(base_url + f'/{group_url}/events/{event_id}/rsvps', params=payload, timeout=3)
        print(res.url)
        member_ids = []
        if res.status_code == 200:
            res_content = json.loads(res.content)
            len(res_content)
            if len(res_content) is not 0:
                for rsvp in res_content:
                    member_dict = {'id': rsvp.get('member').get('id'), 'name': rsvp.get('member').get('name')}
                    member_ids.append(member_dict)
                return member_ids
        elif res.status_code == 400:
            raise InvalidURL
        elif res.status_code == 404:
            raise ConnectionError(res.status_code)
    except (HTTPError, InvalidURL, ConnectionError) as e:
        res_content = json.loads(res.content)
        log.error(res_content['errors'][0]['message'])


def get_member_ids(group_url: str, api_key: str, base_url='https://api.meetup.com'):
    """
    This method returns the member meetup_ids for the specified group url.
    :param group_url: url-name of the group as available on meetup.com
    :param api_key: API Key of the user polling the event_id using this function
    :param base_url: base url of the meetup API, default_url = 'https://api.meetup.com/'
    :returns: list of member_id:name if the group url is valid and the group has members, None, otherwise.
    """
    payload = {'key': api_key}
    try:
        res = requests.get(base_url + f'/{group_url}/members', params=payload, timeout=3)
        if res.status_code == 200:
            res_content = json.loads(res.content)
            member_ids = []
            if len(res_content) is not 0:
                for member in res_content:
                    member_dict = {'id': member.get('id'), 'name': member.get('name')}
                    member_ids.append(member_dict)
            else:
                None
        elif res.status_code == 400:
            raise InvalidURL
        elif res.status_code == 404:
            raise ConnectionError(res.status_code)
    except (HTTPError, InvalidURL, ConnectionError) as e:
        res_content = json.loads(res.content)
        log.error(res_content['errors'][0]['message'])


# Basic flow for meetup id polls
API_KEY = 'your_api_key'  # Available on: https://secure.meetup.com/meetup_api/key/
GROUP_NAME = 'group-url'  # Available on: https://www.meetup.com
event_id = has_an_event(GROUP_NAME, api_key=API_KEY)
if event_id is not None:
    group_member_ids = get_member_ids(GROUP_NAME, api_key=API_KEY)
    rsvp_ids = poll_rsvps(GROUP_NAME, event_id, API_KEY)
else:
    print("The group has no new events")

# USER_MEETUP_ID = 'your_meetup_id'                # Available on: https://www.meetup.com (profile section)
# is_valid_member = is_valid_member_id(member_id=USER_MEETUP_ID, api_key=API_KEY)
# if is_valid_member:
#     is_group_member = is_part_of_group(member_id=USER_MEETUP_ID, api_key=API_KEY)
# else:
#     log.info(f"ID: {USER_MEETUP_ID} is not a valid meetup id")

# Example:-

# GROUP_NAME_PYTHON_PUNE = 'PythonPune'
# GROUP_NAME_OTHER = 'GroupArtCircle'
# INCORRECT_MEETUP_ID = "loremipsum"
# INCORRECT_GROUP_NAME = "sbchjdbchjbsdhbc"


# print("Checking Meetup ID exists")
# print(is_valid_member_id(member_id=MEETUP_ID, api_key=API_KEY))
# print("Member ID exists and not a part of group")
# print(is_part_of_group(member_id=MEETUP_ID, group_name=GROUP_NAME_OTHER, api_key=API_KEY))
# print("Member ID exists and part of group")
# print(is_part_of_group(member_id=MEETUP_ID, group_name=GROUP_NAME_PYTHON_PUNE, api_key=API_KEY))
# print("Wrong Member ID and correct group name")
# print(is_part_of_group(member_id=INCORRECT_MEETUP_ID, group_name=GROUP_NAME_PYTHON_PUNE, api_key=API_KEY))
# print("Correct Member ID and correct group name incorect")
# print(is_part_of_group(member_id=MEETUP_ID, group_name=INCORRECT_GROUP_NAME, api_key=API_KEY))
